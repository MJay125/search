int linearSerch (auto Data, auto key)
{
	for (int i=0; i<Data.size(); i++)
	{
		if (Data[i]==key)//looks for the pattern
		{
			return i; //return the text index if key is found
		}//end if
	}//end for
	return -1; //element not found
}//end programme
